use anyhow::*;
use reqwest::Client;
use serde_derive::*;
use serde_json as json;

#[tokio::main]
async fn main() -> Result<()> {
    // options
    let Opt {
        id: tweet_id,
        secret: secret_path,
    } = Opt::from_args();

    // get secrets
    let secret_toml = std::fs::read_to_string(secret_path)?;
    let secret: Secret = toml::from_str(&secret_toml)?;

    // create client for HTTP requests
    let client = Client::new();

    // get access token
    let token = get_token(&client, &secret).await?;

    // get tweet by id
    let tweet = get_tweet(&client, &token, tweet_id).await?;
    let author_handle = tweet["user"]["screen_name"]
        .as_str()
        .expect("user screen name");
    let author_name = tweet["user"]["name"].as_str().expect("user name");
    let timestamp = tweet["created_at"].as_str().expect("created at");
    let contents = tweet["text"].as_str().expect("text");

    let bib_id = format!(
        "{}_{}",
        escape_bibtex(author_handle),
        tweet_id
    );
    let bib_title = format!("TITLE = {{{}}}", escape_bibtex(contents));
    let bib_author = format!(
        "AUTHOR = {{{} (https://twitter.com/{})}}",
        escape_bibtex(author_name),
        escape_bibtex(author_handle)
    );
    let bib_note = format!(
        "NOTE = {{URL:https://twitter.com/{}/status/{} (version: {})}}",
        escape_bibtex(author_handle),
        tweet_id,
        escape_bibtex(timestamp)
    );
    let bib_eprint = format!(
        "EPRINT = {{https://twitter.com/{}/status/{}}}",
        author_handle,
        tweet_id,
    );
    let bib_url = format!(
        "URL = {{https://twitter.com/{}/status/{}}}",
        author_handle,
        tweet_id,
    );
    // generate bibtex entry

    let bibtex = format!(
        "
@MISC {{{},
    {},
    {},
    HOWPUBLISHED = {{Twitter}},
    {},
    {},
    {}
}}
",
        bib_id, bib_title, bib_author, bib_note, bib_eprint, bib_url,
    );
    println!("{}", bibtex);

    Ok(())
}

fn escape_bibtex<S: AsRef<str>>(text: S) -> String {
    // NOTE: Bibtex might not like arbitrary strings.
    //       We are trying to escape at least *some* cases.
    text.as_ref()
        .replace('#', "\\#")
        .replace('{', "\\{")
        .replace('}', "\\}")
        .replace('_', "\\_")
}

//
// Commandline Argument
//

use std::path::PathBuf;
use structopt::StructOpt;
#[derive(Clone, StructOpt)]
#[structopt(name = "tweet-to-bibtex-entry")]
struct Opt {
    id: usize,
    #[structopt(short = "s", long, parse(from_os_str), default_value = "secret.toml")]
    secret: PathBuf,
}

//
// Credentials
//

#[derive(Clone, Deserialize)]
pub struct Secret {
    api_key: String,
    api_secret_key: String,
    access_token: String,
    access_token_secret: String,
}

#[derive(Clone, Deserialize)]
pub struct Token {
    token_type: String,
    access_token: String,
}

/// https://developer.twitter.com/en/docs/basics/authentication/oauth-2-0/bearer-tokens
pub async fn get_token(client: &Client, secret: &Secret) -> Result<Token> {
    Ok(client
        .post("https://api.twitter.com/oauth2/token")
        .header("Content-Type", "application/x-www-form-urlencoded")
        .basic_auth(&secret.api_key, Some(&secret.api_secret_key.to_owned()))
        .body("grant_type=client_credentials")
        .send()
        .await?
        .json::<Token>()
        .await?)
}

//
// Twitter API
//

/// https://developer.twitter.com/en/docs/tweets/post-and-engage/api-reference/get-statuses-show-id
pub async fn get_tweet(client: &Client, token: &Token, id: usize) -> Result<json::Value> {
    Ok(client
        .get("https://api.twitter.com/1.1/statuses/show.json")
        .query(&[("id", id)])
        .bearer_auth(&token.access_token)
        .send()
        .await?
        .json()
        .await?)
}
